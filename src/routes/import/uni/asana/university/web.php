<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportUniAsana\app\http\controllers\ImportUniAsanaUniversityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('import/uni/asana/university/create',
    [ImportUniAsanaUniversityController::class, 'create'])
    ->name('imports.uni.asana.universities.create');

Route::post('import/uni/asana/university/',
    [ImportUniAsanaUniversityController::class, 'store'])
    ->name('imports.uni.asana.universities.store');
