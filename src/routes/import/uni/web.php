<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportUniAsana\app\http\controllers\ImportUniAsanaUniversityController;
use Yeltrik\ImportUniAsana\app\http\controllers\ImportUniController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('import/uni',
    [ImportUniController::class, 'index'])
    ->name('imports.uni.index');

require 'asana/web.php';
