@extends('layouts.app')

@section('content')
<div class="container">

    <h2>
        Import University
    </h2>

    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Import University
        </a>
        <a href="{{route('imports.uni.asana.universities.create')}}" class="list-group-item list-group-item-action">University Import Form</a>
    </div>

</div>
@endsection
