@extends('layouts.app')

@section('content')
    <div class="container">

        <h3>
            University Import from Asana Form
        </h3>

        @include("importUniAsana::import.uni.asana.university.forms.import")

    </div>
@endsection
