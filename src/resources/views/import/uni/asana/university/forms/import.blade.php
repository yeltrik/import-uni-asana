<form
    method="POST"
    action="{{ action([\Yeltrik\ImportUniAsana\app\http\controllers\ImportUniAsanaUniversityController::class, 'store']) }}"
    enctype="multipart/form-data"
>
    @csrf

    @include('importUniAsana::import.uni.asana.university.inputs.import_by_asana_pd_project_gid')
    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.import_by_asana_export_csv_file')
    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.import_by_asana_export_json_file')
    <br>

    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.import_new_universities')
    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.update_existing_universities')
    <br>

    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.import_colleges')
    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.import_departments')
    <br>

    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.import_abbrs')
    <br>

    <br>
    @include('importUniAsana::import.uni.asana.university.inputs.import_button')
</form>
