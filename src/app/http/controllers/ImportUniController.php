<?php

namespace Yeltrik\ImportUniAsana\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ImportUniController extends Controller
{
    /**
     * ImportUniController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        // TODO: Auth

        return view('importUniAsana::import.uni.index');
    }

}
