<?php

namespace Yeltrik\ImportUniAsana\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\ImportUniAsana\app\import\university\AsanaUniUniversityCsvImporter;

class ImportUniAsanaUniversityController extends Controller
{

    /**
     * ImportUniAsanaUniversityController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        // TODO Authentication

        return view("importUniAsana::import.uni.asana.university.create");
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws FileNotFoundException
     */
    public function store(Request $request)
    {
        $request->validate([
            'import_method' => 'required',
        ]);

        switch($request->import_method) {
            case "export_csv":
                AsanaUniUniversityCsvImporter::validate($request);
                $importer = new AsanaUniUniversityCsvImporter($request);
                return $importer->process();
            default:
                dd('Unsupported Import Method: ' . $request->import_method);
        }
    }

}
