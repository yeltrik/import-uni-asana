<?php


namespace Yeltrik\ImportUniAsana\app\import\university;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaCsv;
use Yeltrik\ImportUniAsana\app\import\college\AsanaUniCollegeCsvImporter;
use Yeltrik\ImportUniAsana\app\import\department\AsanaUniDepartmentCsvImporter;
use Yeltrik\UniOrg\app\models\University;

class AsanaUniUniversityCsvImporter extends Abstract_ImportAsanaCsv
{

    const FILE_PROPERTY_NAME = "asana_export_csv_file";

    public function __construct(Request $request)
    {
        parent::__construct($request, $this->initCsv($request));
    }

    /**
     * @param Request $request
     * @return array
     * @throws FileNotFoundException
     */
    public function initCsv(Request $request)
    {
        $propertyName = static::FILE_PROPERTY_NAME;
        $fileName = time() . '_' . $request->$propertyName->getClientOriginalName();
        $filePath = $request->file(static::FILE_PROPERTY_NAME)->storeAs('uploads', $fileName, 'public');

        $data = Storage::disk('public')->get($filePath);
        $bom = pack('CCC', 0xEF, 0xBB, 0xBF);
        if (strncmp($data, $bom, 3) === 0) {
            $data = substr($data, 3);
        }
        $rows = explode("\n", $data);
        return $this->associateCsv($rows);
    }

    private function associateCsv($rows)
    {
        $array = array_map('str_getcsv', $rows);
        array_walk($array, function (&$a) use ($array) {
            if (sizeof($array[0]) === sizeof($a)) {
                $a = array_combine($array[0], $a);
            }
        });
        array_shift($array); # remove column header
        return $array;
    }

    /**
     * @return RedirectResponse
     * @throws FileNotFoundException
     */
    public function process()
    {
        if (
            $this->request()->import_new_universities ||
            $this->request()->update_existing_universities
        ) {
            $this->processCsv();
        }
        if ($this->request()->import_colleges) {
            $asanaUniCollegeCsvImporter = new AsanaUniCollegeCsvImporter($this->request(), $this->csv());
            $asanaUniCollegeCsvImporter->process();
        }
        if ($this->request()->import_departments) {
            $asanaUniDepartmentCsvImporter = new AsanaUniDepartmentCsvImporter($this->request(), $this->csv());
            $asanaUniDepartmentCsvImporter->process();
        }

//        dd(['process done', $this->request()->toArray()]);

        return back()
            ->with('success', 'You have successfully upload file.');
    }

    /**
     * @throws FileNotFoundException
     */
    private function processCsv()
    {
        foreach ($this->csv() as $row) {
            if (isset($row['Projects'])) {
                if ($row['Projects'] === 'Universities') {
                    if (
                        $this->request()->import_new_universities &&
                        $this->universityIsNew($row)
                    ) {
                        //$gid = $row['Task ID'];
                        $name = $row['Name'];
                        $abbr = $row['Abbr'];
                        $university = new University();
                        $university->name = $name;
                        if ($this->request()->import_abbrs) {
                            $university->abbr = $abbr;
                        }
                        $university->save();
                    }
                    if (
                        $this->request()->update_existing_universities &&
                        $this->universityExists($row)
                    ) {
                        $gid = $row['Task ID'];
                        $name = $row['Name'];
                        $abbr = $row['Abbr'];
                        $university = University::query()
                            ->where('name', '=', $name)
                            ->first();
                        if ($university instanceof University) {
                            if ($this->request()->import_abbrs) {
                                $university->abbr = $abbr;
                            }
                            $university->save();
                        }
                    }
                }
            } else {
                dd([
                    'Unsupported Row',
                    $row
                ]);
            }
        }

    }

    /**
     * @param array $row
     * @return bool
     */
    private function universityExists(array $row)
    {
        $name = $row['Name'];
        $university = University::query()
            ->where('name', '=', $name)
            ->first();

        return ($university instanceof University);
    }

    /**
     * @param array $row
     * @return bool
     */
    private function universityIsNew(array $row)
    {
        return !$this->universityExists($row);
    }

    /**
     * @param Request $request
     */
    public static function validate(Request $request)
    {
        // Validate only if Option was to Upload
        $request->validate([
            'asana_export_csv_file' => 'required|mimes:txt|max:20480',
        ]);
    }

}
