<?php


namespace Yeltrik\ImportUniAsana\app\import\college;


use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\University;

class AsanaUniCollegeCreator extends Abstract_AsanaUniCollege
{

    public function process()
    {
        if ($this->collegeIsNew($this->row())) {
            $name = $this->row()['Name'];
            $abbr = $this->row()['Abbr'];
            $universityName = $this->row()['Parent Task'];
            $college = new College();
            $college->name = $name;
            if ($this->request()->import_abbrs) {
                $college->abbr = $abbr;
            }
            $university = University::query()
                ->where('name', '=', $universityName)
                ->first();
            if ($university instanceof University) {
                $college->university()->associate($university);
                $college->save();
            } else {
                dd([
                    'University does not exist',
                    $universityName
                    ]);
            }
        }
    }

}
