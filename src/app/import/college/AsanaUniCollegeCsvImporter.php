<?php


namespace Yeltrik\ImportUniAsana\app\import\college;


use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaCsv;
use Yeltrik\UniOrg\app\models\College;

class AsanaUniCollegeCsvImporter extends Abstract_ImportAsanaCsv
{

    /**
     *
     */
    public function process()
    {
        if ($this->request()->import_colleges) {
            foreach ($this->csv() as $row) {
                if (isset($row['Projects'])) {
                    if ($row['Projects'] === 'Colleges') {
                        if ($this->collegeIsNew($row)) {
                            $asanaUniCollegeCreator = new AsanaUniCollegeCreator($this->request(), $row);
                            $asanaUniCollegeCreator->process();
                        } else {
                            $asanaUniCollegeUpdater = new AsanaUniCollegeUpdater($this->request(), $row);
                            $asanaUniCollegeUpdater->process();
                        }
                    }
                }
            }
        }
    }

    /**
     * @param array $row
     * @return bool
     */
    private function collegeExists(array $row)
    {
        $name = $row['Name'];
        $university = College::query()
            ->where('name', '=', $name)
            ->first();

        return ($university instanceof College);
    }

    /**
     * @param array $row
     * @return bool
     */
    private function collegeIsNew(array $row)
    {
        return !$this->collegeExists($row);
    }

}
