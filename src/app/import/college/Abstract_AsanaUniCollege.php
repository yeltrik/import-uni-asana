<?php


namespace Yeltrik\ImportUniAsana\app\import\college;


use Illuminate\Http\Request;
use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaCsvRow;
use Yeltrik\UniOrg\app\models\College;

abstract class Abstract_AsanaUniCollege extends Abstract_ImportAsanaCsvRow
{

    /**
     * @param array $row
     * @return bool
     */
    protected function collegeExists(array $row)
    {
        $name = $row['Name'];
        $university = College::query()
            ->where('name', '=', $name)
            ->first();

        return ($university instanceof College);
    }

    /**
     * @param array $row
     * @return bool
     */
    protected function collegeIsNew(array $row)
    {
        return !$this->collegeExists($row);
    }

}
