<?php


namespace Yeltrik\ImportUniAsana\app\import\department;


use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaCsvRow;
use Yeltrik\UniOrg\app\models\Department;

class Abstract_AsanaUniDepartment extends Abstract_ImportAsanaCsvRow
{

    /**
     * @param array $row
     * @return bool
     */
    protected function departmentExists(array $row)
    {
        $name = $row['Name'];
        $department = Department::query()
            ->where('name', '=', $name)
            ->first();

        return ($department instanceof Department);
    }

    /**
     * @param array $row
     * @return bool
     */
    protected function departmentIsNew(array $row)
    {
        return !$this->departmentExists($row);
    }

}
