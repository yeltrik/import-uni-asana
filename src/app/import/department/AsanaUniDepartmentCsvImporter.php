<?php


namespace Yeltrik\ImportUniAsana\app\import\department;


use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaCsv;
use Yeltrik\UniOrg\app\models\Department;

class AsanaUniDepartmentCsvImporter extends Abstract_ImportAsanaCsv
{

    public function process()
    {
        if ($this->request()->import_departments) {
            foreach ($this->csv() as $row) {
                if (isset($row['Projects'])) {
                    if ($row['Projects'] === 'Departments') {
                        if ($this->departmentIsNew($row)) {
                            $asanaUniDepartmentCreator = new ImportAsanaUniDepartmentCreator($this->request(), $row);
                            $asanaUniDepartmentCreator->process();
                        } else {
                            $asanaUniDepartmentUpdater = new ImportAsanaUniDepartmentUpdater($this->request(), $row);
                            $asanaUniDepartmentUpdater->process();
                        }
                    }
                }
            }
        }
    }

    /**
     * @param array $row
     * @return bool
     */
    private function departmentExists(array $row)
    {
        $name = $row['Name'];
        $department = Department::query()
            ->where('name', '=', $name)
            ->first();

        return ($department instanceof Department);
    }

    /**
     * @param array $row
     * @return bool
     */
    private function departmentIsNew(array $row)
    {
        return !$this->departmentExists($row);
    }

}
