<?php


namespace Yeltrik\ImportUniAsana\app\import\department;


use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;

class ImportAsanaUniDepartmentUpdater extends Abstract_AsanaUniDepartment
{

    public function process()
    {
        if (isset($this->row()['Projects'])) {
            if ($this->row()['Projects'] === 'Departments') {
                if ($this->departmentExists($this->row())) {
                    $name = $this->row()['Name'];
                    $abbr = $this->row()['Abbr'];
                    $collegeName = $this->row()['Parent Task'];
                    $department = Department::query()
                        ->where('name', '=', $name)
                        ->first();
                    if ($department instanceof Department) {
                        if ($this->request()->import_abbrs) {
                            $department->abbr = $abbr;
                        }
                        $college = College::query()
                            ->where('name', '=', $collegeName)
                            ->first();
                        if ($college instanceof College) {
                            $department->college()->associate($college);
                        }
                        $department->save();
                    }
                }
            }
        }
    }

}
